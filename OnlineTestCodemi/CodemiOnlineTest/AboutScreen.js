
import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export default class App extends React.Component {
    render(){
          return (
            <View style={styles.container}>
                <View style={styles.Judul}>
                    <Text style={{fontSize: 36, color:'#003366', padding: 20}}>Tentang Saya</Text>
                    <Icon name="user-circle" size={120} color="#003366" style={{padding: 10}}/>
                    <Text style={{fontSize: 20, fontWeight:'bold', color:'#003366' }}>M. Ali Toha</Text>
                    <Text style={{fontSize: 18, color:'#3EC6FF' }}>React Native Developer</Text>
                </View>

                <View style={styles.about}>
                    <View style={styles.boxIsiPortofolio}>
                        <View style= {{justifyContent:'flex-start', padding:7}}>
                            <Text style={{fontWeight:'bold', color:'#003366'}}>Portofolio</Text>
                            <View style={{borderWidth:1, color:'#003366'}}></View>
                        </View>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                                <Icon name='gitlab' size={30} color='#3EC6FF'/>
                                <Text style={{fontWeight:'bold', color:'#3EC6FF'}}>@ALiilili</Text>
                            </View>
                            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                                <Icon name='github' size={30} color='#3EC6FF'/>
                                <Text style={{fontWeight:'bold', color:'#3EC6FF'}}>@TOhAlii</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={styles.hubungi}>
                    <View style={styles.boxIsiPortofolio}>
                        <View style= {{justifyContent:'flex-start', padding:7}}>
                            <Text style={{fontWeight:'bold', color:'#003366'}}>Hubungi Saya</Text>
                            <View style={{borderWidth:1, color:'#003366'}}></View>
                        </View>
                        <View style={{flex:1, alignItems:'center', padding:5}}>
                            <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center', padding:5}}>
                                <Icon name='facebook' size={30} color='#3EC6FF'/>
                                <Text style ={{marginStart:5, fontWeight:'bold', color:'#3EC6FF'}}>@tahuganteng</Text>
                            </View>
                            <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center', padding:5}}>
                                <Icon name='instagram' size={30} color='#3EC6FF'/>
                                <Text style ={{marginStart:5, fontWeight:'bold', color:'#3EC6FF'}}>@aaaallii</Text>
                            </View>
                            <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems:'center', padding:5}}>
                                <Icon name='twitter' size={30} color='#3EC6FF'/>
                                <Text style ={{marginStart:5, fontWeight:'bold', color:'#3EC6FF'}}>@ahotila</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
  );}
}


const styles = StyleSheet.create({
  container: {
    flex:1,
    marginTop: 20,
  },
  Judul:{
    flex:3,
    justifyContent: "flex-start",
    alignItems:'center',
  },
  about:{
    flex:1
  },
  hubungi:{
    flex:2
  },

  boxIsiPortofolio:{
    flex:1,
    margin: 5, 
    borderRadius :100/10, 
    backgroundColor:'#EFEFEF'
  },
  

});


